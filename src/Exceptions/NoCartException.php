<?php
namespace FruitStore\Exceptions;
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 11.01.2019
 * Time: 14:18
 */
class NoCartException  extends \Exception{
    public function __construct()
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $this->message="\n". "\e[1;33;40mError:No cart available!\e[0m\n"."\n";
        parent::__construct();
    }

}