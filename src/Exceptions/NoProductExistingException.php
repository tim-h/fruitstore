<?php
namespace FruitStore\Exceptions;
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 09.01.2019
 * Time: 11:56
 */
class NoProductExistingException extends \Exception {
    public function __construct()
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $this->message="\n". "\e[1;33;40mError: There are not products in the shop OR you chose a not existing product!\e[0m\n"."\n";
        parent::__construct();
    }
}