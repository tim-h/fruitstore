<?php
namespace FruitStore\Models;
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 10.01.2019
 * Time: 07:59
 */
abstract class AbstractArticle{
    protected $name;
    protected $price;
    protected $id;
    protected $quantity;

    public function __construct($id, $name,$price, $quantity){
        $this->name=$name;
        $this->price=$price;
        $this->id=$id;
        $this->quantity=$quantity;
    }
    public function getName(){
        return $this->name;
    }
    public function getPrice(){
        return $this->price;
    }
    public function getId(){
        return $this->id;
    }
    public function setName(string $name){
        $this->name=$name;
        return $this;
    }
    public function increasePrice($price){
        $this->price+=$price;
    }
    public function increaseQuantity($q){
        $this->quantity+=$q;
    }
    public function setPrice(int $price){
        $this->price=$price;
        return $this;
    }
    public function setId( $id){
        $this->id=$id;
        return $this;
    }
    public function getQuantity(){
        return $this->quantity;
    }
    public function setQuantity( $q){
        $this->quantity=$this->quantity+$q;
        return $this;
    }

}