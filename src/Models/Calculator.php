<?php
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 18.01.2019
 * Time: 14:53
 */

namespace FruitStore\Models;


class Calculator
{
    protected $bit=[];
    protected $bitAmount;
    protected $spinTime;

    public function __construct(){
        $this->bitAmount=1000;
        for($i=0;$i<$this->bitAmount;$i++){
            $this->bit[$i]=0;
        }
    }
    public function controll(){
        foreach($this->bit as $key => $value){
            if($this->bit[$key]==2){
                $this->bit[$key]=0;
                $this->bit[$key+1]+=1;
            }
        }
    }

    public function decToBin(int $n){
        if($n>-1){
            for($i=0;$i<$n;$i++){
                $this->bit[0]+=1;
               $this->controll();
            }
            $time_pre2 = microtime(true);
            $this->bit=array_reverse($this->bit);
            $time_post2 = microtime(true);
            $this->spinTime = $time_post2 - $time_pre2;
        }
    }

    public function show(){
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';// clear cmd
       $input= readline("Type in dec-number: ");
       $input= (int)$input;
        $time_pre = microtime(true);
        $this->decToBin($input);
        $time_post = microtime(true);
        $exec_time = $time_post - $time_pre;
        echo"\n";
        echo "Your input: ".$input."\n";
        echo"\n";
        echo "Output: ";
        $i=0;
        $time_pre2 = microtime(true);
      while( $this->bit[$i]!=1){
          unset($this->bit[$i]);
          $i++;
      }
        $time_post2 = microtime(true);
        $exec_time2 = $time_post - $time_pre;
       foreach($this->bit as $key=>$value){
           echo $value;
       }
        echo"\n";
        echo"\n";
       echo"Needed time to get bin-number: ".$exec_time."s";
        echo"\n";
        echo"\n";
        echo"Needed time to remove unneccessary numbers: ".$exec_time2."s";
        echo"\n";
        echo"\n";
        echo"Needed time to spin array: ".$this->spinTime."s";
        echo"\n";
        echo"\n";
    }

}