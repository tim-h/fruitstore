<?php

namespace FruitStore\Models;

use FruitStore\Exceptions\NoCartException;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 11.01.2019
 * Time: 14:06
 */
 class Cart
{
    protected $cartNbr;

    protected $content = [];
    protected $amountCarts = 100;

    public function inCartProduct($id)
    {
        foreach ($this->content as $key => $value) {
            if ($value->getId() === $id) {
                return true;
                break 1;
            }
        }
        return false;
    }

    public function getAmountCarts()
    {
        return $this->amountCarts;
    }

    public function setCartNbr($a)
    {
        $this->cartNbr = $a;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent(AnyProduct $p)
    {
        $this->content[$p->getId()] = $p;
    }

    public function removeContent($index, $a)
    {
        $a *= -1;
        $this->content[$index]->setQuantity($a);
        if ($this->content[$index]->getQuantity() <= 0) {
            unset($this->content[$index]);
        }
    }

    public function deleteWholeProduct($index)
    {
        $this->content[$index] = null;
    }

    public function getCartNbr()
    {
        return $this->cartNbr;
    }


}
