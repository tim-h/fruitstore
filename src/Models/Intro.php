<?php
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 18.01.2019
 * Time: 12:04
 */

namespace FruitStore\Models;


class Intro
{
    public function intro(){
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';// clear cmd
        echo"\n";
        echo "  ______          _ _    _____ _                 "."\n";
        echo " |  ____|        (_) |  / ____| |                "."\n";
        echo " | |__ _ __ _   _ _| |_| (___ | |__   ___  _ __  "."\n";
        echo " |  __| '__| | | | | __|\___ \| '_ \ / _ \| '_ \ "."\n";
        echo " | |  | |  | |_| | | |_ ____) | | | | (_) | |_) |"."\n";
        echo " |_|  |_|   \__,_|_|\__|_____/|_| |_|\___/| .__/ "."\n";
        echo "                                          | |    "."\n";
        echo "                                          |_|    "."\n";
        echo"\n"."\n"."\n";
        echo"(To choose a certain option, press the correct number and enter)"."\n";
        sleep(4);
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';// clear cmd
    }
    public function header(){

        echo"\n";
        echo "  ___         _ _   ___ _             "."\n";
        echo " | __| _ _  _(_) |_/ __| |_  ___ _ __ "."\n";
        echo " | _| '_| || | |  _\__ \ ' \/ _ \ '_ \ "."\n";
        echo " |_||_|  \_,_|_|\__|___/_||_\___/ .__/"."\n";
        echo "                                |_|   "."\n";
        echo"\n";
    }

}


