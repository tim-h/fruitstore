<?php

namespace FruitStore\Models;

use FruitStore\Exceptions\DuplicateProductException;
use FruitStore\Exceptions\EmtpyShoppingListException;
use FruitStore\Exceptions\EmptyShopException;
use FruitStore\Exceptions\NoCartException;
use FruitStore\Exceptions\NoProductExistingException;
use FruitStore\Exceptions\NoIdException;
use FruitStore\Exceptions\EmptyListException;
use FruitStore\Exceptions\EmptyCartException;
use FruitGarden\Models;
use FruitStore\Exceptions\UnexpectedNumberException;
use FruitStore\Exceptions\SoldOutException;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 10.01.2019
 * Time: 08:00
 */
abstract class Shop
{
    protected $list = []; // of all products in shop
    protected $empty = ' ';
    protected $shopName;
    protected $street;
    protected $houseNumber;
    protected $city;
    protected $cityCode;
    protected $phone;
    protected $where = 0;
    protected $cart = [];
    protected $backMoney;
    protected $checkout;
    protected $head;

    /**
     * Shop constructor.
     * @param $shopName
     * @param $street
     * @param $houseNumber
     * @param $city
     * @param $cityCode
     * @param $phone
     */
    public function __construct($shopName, $street, $houseNumber, $city, $cityCode, $phone)
    {
        $this->houseNumber = $houseNumber;
        $this->street = $street;
        $this->city = $city;
        $this->shopName = $shopName;
        $this->cityCode = $cityCode;
        $this->phone = $phone;
        $this->checkout = new Checkout();
        $this->head=new Intro();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s%s%s%s%s%s %s%s%s %s%s%s%s%s%s%s',
            PHP_EOL,
            "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m",
            PHP_EOL,
            $this->shopName,
            PHP_EOL,
            $this->street,
            $this->houseNumber,
            PHP_EOL,
            $this->cityCode,
            $this->city,
            PHP_EOL,
            $this->phone,
            PHP_EOL,
            "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m",
            PHP_EOL,
            PHP_EOL
        );
    }

    /**
     * @return mixed
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return mixed
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCityCode()
    {
        return $this->cityCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws NoProductExistingException
     */
    public function isProductAvailable($id): ?bool
    {
        if (isset($this->list[$id]) === false || $this->list[$id]->getQuantity() <= 0) {
            throw new NoProductExistingException();
        } else {
            return true;
        }
    }

    /**
     * @param $crtnmbr
     * @return Shop
     * prints the selected cart input
     */
    public function showCartInput($crtnmbr)
    {
        try {
            if (empty($this->cart[$crtnmbr]->getContent() == true)) {
                throw new EmptyCartException();
            } else {
                echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                $this->head->header();
                echo "\n";
                echo "\e[1;36;40mYOUR CART: \e[0m\n" . "\n";
                echo "############################################################" . "\n";
                echo 'Cart includes...' . "\n";
                foreach ($this->cart[$crtnmbr]->getContent() as $key => $value) {
                    echo "[" . $value->getId() . "]  ";
                    echo "Name: " . $value->getName() . ", ";
                    echo "Price: " . $value->getPrice() . ", ";
                    echo "Quantity: " . $value->getQuantity() . "\n";
                }
                echo "############################################################" . "\n";

                return $this;
            }
        } catch (EmptyCartException $e) {
            var_export($e->getMessage());
            $this->menue($crtnmbr);
        }
    }

    /**
     * @param $crtnmbr
     * presents the bill´s head
     */
    public function showBill($crtnmbr)
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        echo "\n";
        echo "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m\n" . "\n";
        echo $this->shopName . "\n";
        echo $this->street . $this->empty . $this->houseNumber . "\n";
        echo $this->cityCode . $this->empty . $this->city . "\n";
        echo $this->phone . "\n";
        $this->showCartInput($crtnmbr);
        echo "\n" . "Payable amount: " . $this->getPriceOfAll($crtnmbr) . '$' . "\n";

        echo "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m\n" . "\n";
    }

    /**
     *interactive search for an article by name
     */
    public function searchProductInteractive()
    {
        try {
            if (empty($this->list) == true) {
                throw new EmptyListException();
            } else {
                echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                echo "\n";
                $this->head->header();
                echo "\e[1;36;40mSEARCH THROUGH STOCK \e[0m\n" . "\n";
                echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" . "\n";
                echo "\e[0;30;47mTip in the product´s name: \e[0m\n" . "\n";
                $entry = readline("Your entry:");
                $where = null;
                $found = false;
                foreach ($this->list as $key => $value) {
                    if ($value->getName() === $entry) {
                        $where = $key;
                        $found = true;
                    }
                }
                if ($found == true) {
                    echo "\n";
                    echo "Your searched product: " . "\n";
                    echo "\n";
                    echo "ID: " . $this->list[$where]->getId() . "  Name: " . $this->list[$where]->getName() . "  Price: " . $this->list[$where]->getPrice() . "  Quantity: " . $this->list[$where]->getQuantity() . "\n";
                    echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" . "\n";
                    echo "\n";
                } else {
                    throw new NoProductExistingException();
                }
            }
        } catch (NoProductExistingException $n) {
            var_export($n->getMessage());
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }

    public function deleteProductInteractive()
    {
        try {
            if (empty($this->list) == true) {
                throw new EmptyListException();
            } else {
                echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                echo "\n";
                $this->head->header();
                echo "\e[1;36;40mMANAGE STOCK \e[0m\n" . "\n";
                echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" . "\n";
                echo "\e[0;30;47mChoose product to delete: \e[0m\n" . "\n";
                echo "\n";
                $this->showAllProductsInLine();
                echo "\n";
                $entry = readline("Your entry:");
                $where = null;
                $found = false;
                foreach ($this->list as $key => $value) {
                    if ($value->getId() === $entry) {
                        $where = $key;
                        $found = true;
                    }
                }
                if ($found == true) {
                    echo "\n";
                    echo "Product " . $this->list[$where]->getName() . " deleted!" . "\n";
                    echo "\n";
                    echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" . "\n";
                    echo "\n";
                    unset($this->list[$where]);
                } else {
                    throw new NoProductExistingException();
                }
            }
        } catch (NoProductExistingException $n) {
            var_export($n->getMessage());
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }

    /**
     * @param $crtnmbr
     * @return float|int
     * returns the absolute price
     */
    public function getPriceOfAll($crtnmbr)
    {
        $cash = 0;
        foreach ($this->cart[$crtnmbr]->getContent() as $key => $value) {
            $casi = $value->getPrice() * $value->getQuantity();
            $cash += $casi;
        }
        return $cash;
    }

    /**
     * @param $cartNumber
     * shows complete bill including every other cart
     */
    public function showBill2($cartNumber)
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        if (empty($this->cart[$cartNumber]->getContent() === true)) {
            $products = [];
            foreach ($this->cart[$cartNumber]->getContent() as $key => $value) {
                $prod = new AnyProduct($value->getId(), $value->getName(), $value->getPrice(), $value->getQuantity());
                $products[$value->getId()] = $prod;
            }
            echo "\n";
            echo "\e[1;36;40mCHECKOUT: \e[0m\n" . "\n";
            echo "\e[0;30;47mWant you to pay for another cart?  \e[0m\n" . "\n";
            echo "\n";
            echo "[1] Yes" . "\n" . "[2] No" . "\n";
            echo "\n";
            $choice = readline("Your entry:");
            while (2 < $choice || $choice < 1) {
                $choice = readline("Your entry:");
            }
            switch ($choice) {
                case 2:
                    {
                        $this->showBill($cartNumber);
                        break;
                    }
                case 1:
                    {
                        if (empty($this->cart == false)) {
                            echo "\e[0;30;47mSelect a cart: \e[0m\n" . "\n";
                            foreach ($this->cart as $key => $value) {
                                if ($value->getCartNbr() != $cartNumber) {
                                    echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                                    echo "[" . $value->getCartNbr() . "]" . "\n";
                                }
                            }
                            $choice2 = readline("Your Entry: " . "\n");
                            while ($this->isCrtNmbrInArr($choice2) === false || $choice2 === null) {
                                $choice2 = readline("Your Entry: " . "\n");
                            }
                            foreach ($this->cart[$choice2]->getContent() as $key => $value) {
                                $prod2 = new AnyProduct($value->getId(), $value->getName(), $value->getPrice(), $value->getQuantity());
                                if ($products[$value->getId()] == null) {
                                    $products[$value->getId()] = $prod2;
                                } else {
                                    $products[$value->getId()]->increaseQuantity($prod2->getQuantity());
                                }
                            }
                        }
                        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                        echo "\n";
                        echo "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m\n" . "\n";
                        echo $this->shopName . "\n";
                        echo $this->street . $this->empty . $this->houseNumber . "\n";
                        echo $this->cityCode . $this->empty . $this->city . "\n";
                        echo $this->phone . "\n";
                        echo "\n";
                        echo "############################################################" . "\n";
                        echo 'Cart includes...' . "\n";
                        foreach ($products as $key => $value) {

                            echo "[" . $value->getId() . "]  ";
                            echo "Name: " . $value->getName() . ", ";
                            echo "Price: " . $value->getPrice() . ", ";
                            echo "Quantity: " . $value->getQuantity() . "\n";
                        }
                        echo "############################################################" . "\n";
                        $cash = 0;
                        foreach ($products as $key => $value) {
                            $casi = $value->getPrice() * $value->getQuantity();
                            $cash += $casi;
                        }
                        echo "\n" . "Payable amount: " . $cash . '$' . "\n";

                        echo "\e[0;30;47m////////////////////////////////////////////////////////////:  \e[0m\n" . "\n";
                    }
                    break;
            }
        } else {
            echo "Error: There aren´t products in your shopping-list!";
        }
    }

    /**
     *
     */
    public function showAllProductsInLine()
    {
        foreach ($this->list as $key => $value) {
            echo "[" . $value->getId() . "]  ";
            echo "Name: " . $value->getName() . ", ";
            echo "Price: " . $value->getPrice() . ", ";
            echo "Quantity: " . $value->getQuantity() . "\n";
        }
    }

    /**
     * @param $crtnmbr
     * @throws NoProductExistingException
     * searching for duplicates and increases quantity
     */
    public function addArticleToCart($crtnmbr)
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        try {
            if (!empty($this->list)) {
                echo "\n";
                $this->head->header();
                echo "\e[1;36;40mMANAGE CART-INPUT: \e[0m\n" . "\n";
                echo "\e[0;30;47mChoose product you want to buy:  \e[0m\n" . "\n";
                echo "\n";
                $this->showAllProductsInLine();
                echo "\n";
                $input = [];
                $input[0] = readline("Your entry: ");
                while ($input[0] <= 0) {
                    $input[0] = readline("Your entry: ");
                }
                if ($this->isProductAvailable($input[0]) === true) {

                    echo "\n";
                    echo "\e[0;30;47mChose, how often do you want to buy that item:  \e[0m\n" . "\n";
                    echo "\n";
                    $input[1] = readline("Your entry:");
                    while ($input[1] <= 0) {
                        $input[1] = readline("Your entry:");
                    }
                    try {

                        if ($this->list[$input[0]]->getQuantity() >= $input[1]) {
                            $this->decreaseQuantity($input[0], $input[1]);
                            $product = new AnyProduct($this->list[$input[0]]->getId(), $this->list[$input[0]]->getName(), $this->list[$input[0]]->getPrice(), $input[1]);
                            $this->cart[$crtnmbr]->setContent($product);
                        } else {
                            throw new SoldOutException();
                        }
                    } catch (SoldOutException $s) {
                        var_export($s->getMessage());
                    }
                } else {
                    throw new EmptyListException();
                }
                echo "\n";
            }
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }

    /**
     * @param $crtnmbr
     * removes article from cart and adds it back to stock
     */
    public function removeArticleFromCart($crtnmbr)
    {
        try {
            if (empty($this->cart[$crtnmbr]->getContent()) == true) {
                throw new EmptyCartException();
            } else {
                 echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                echo "\n";
                $this->head->header();
                echo "\e[1;36;40mYOUR CART: \e[0m\n" . "\n";
                echo "\e[0;30;47mChoose product you want to remove:  \e[0m\n" . "\n";
                echo "\n";
                foreach ($this->cart[$crtnmbr]->getContent() as $key => $value) {
                    echo "[" . $value->getId() . "]  ";
                    echo "Name: " . $value->getName() . ", ";
                    echo "Price: " . $value->getPrice() . ", ";
                    echo "Quantity: " . $value->getQuantity() . "\n";
                }
                echo "\n";
                $input = [];
                $input[0] = readline("Your entry: ");
                while ($input[0] <= 0) {
                    $input[0] = readline("Your entry: ");
                }
                try {
                    $bool = $this->cart[$crtnmbr]->inCartProduct($input[0]);
                    if ($bool === true) {
                        echo "\n";
                        echo "\e[0;30;47mChoose, how often you want to remove that item  \e[0m\n" . "\n";
                        echo "\n";
                        $input[1] = readline("Your entry:");
                        while ($input[1] <= 0) {
                            $input[1] = readline("Your entry:");
                        }
                        if ($this->list[$input[0]] === null || empty($this->list[$input[0]]) === true) {
                            $this->list[$input[0]] = new AnyProduct($input[0], $this->cart[$crtnmbr]->getContent()[$input[0]]->getName(), $this->cart[$crtnmbr]->getContent()[$input[0]]->getPrice(), 0);
                            $this->list[$input[0]]->setQuantity($input[1]);
                        } else {
                            $this->list[$input[0]]->increaseQuantity($input[1]);
                        }
                        $this->cart[$crtnmbr]->removeContent($input[0], $input[1]);
                    } else {

                        throw new SoldOutException();
                    }
                } catch (SoldOutException $soldOutException) {
                    var_export($soldOutException->getMessage());
                }
            }
        } catch (EmptyCartException $e) {
            var_export($e->getMessage());
            $this->menue($crtnmbr);
        }
    }

    /**
     * @param $id = products id
     * @param $a = amount of article
     * decreases quantity of certain product in stock
     */
    public function decreaseQuantity($id, $a)
    {
        $a *= -1;
        $this->list[$id]->setQuantity($a);
    }

    /**
     * interactive method
     * decreases quantity of certain product in stock
     */
    public function increaseQuantity()
    {
        if ($this->list !== null) {
            echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
            echo "\n";
            $this->head->header();
            echo "\e[1;36;40mSTOCK-MANAGMENT: \e[0m\n" . "\n";
            echo "\e[0;30;47mChoose product to increase quantity:  \e[0m\n" . "\n";

            echo "\n";
            $this->showAllProductsInLine();
            echo "\n";
            $input = [];
            $input[0] = readline("Your entry: ");
            while ($input[0] <= 0) {
                $input[0] = readline("Your entry: ");
            }
            echo "\n";
            echo "Choose quantity, you want to add:" . "\n";
            $input[1] = readline("Your entry:");
            while ($input[1] <= 0) {
                $input[1] = readline("Your entry:");
            }
            $this->list[$input[0]]->setQuantity($input[1]);

        } else {
            echo "Your shop is empty!";
        }
        $this->mainMenu();
    }

    /**
     * proofs that new product is not a duplicate, returns boolean
     *
     * @param string $name
     * @return bool
     */
    public function productExisting(string $name): ?bool
    {
        $this->boolean = true;
        if ($this->list !== null) {
            foreach ($this->list as $key => $value) {
                if ($name === $value->getName()) {
                    return true;
                    break;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @param $nmbr
     * @return bool
     * looking for duplicate cart number
     */
    public function isCrtNmbrInArr($nmbr): ?bool
    {
        if (empty($this->cart) === true) {
            return false;
        } else {
            if ($nmbr >= 0) {
                foreach ($this->cart as $key => $value) {
                    if ($value->getCartNbr() === $nmbr) {
                        return true;
                        break;
                    }
                }
                return false;
            } else {
                echo "Error: Parameter isn´t a number!";
            }
        }
    }

    /**
     * @param $id
     * @return bool
     * looking for duplicate in product ids
     */
    public function idExisting($id)
    {
        $this->boolean = true;
        if ($this->list !== null) {
            foreach ($this->list as $key => $value) {
                if ($id === $value->getId()) {
                    return true;
                    break;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @param $name
     * @param bool $getKey
     * @return $this|int|mixed|string
     */
    public function searchProduct($name, $getKey = false)
    {
        try {
            if ($this->list !== null) {
                foreach ($this->list as $key => $value) {
                    if ($value->getName() === $name) {
                        if ($getKey) {
                            echo $key;
                            return $key;
                        } else {
                            return $value;
                            break;
                        }
                    }
                }
            } else {
                throw new NoProductExistingException();
            }
        } catch (NoProductExistingException $noProductExistingException) {
            var_export($noProductExistingException->getMessage());
        }
        return $this;
    }

    /**
     * @param $article
     * automatically proofs duplicate prod-id
     * occasionally finds a unique id
     */
    public function idControll($article): void
    {
        try {
            if (empty($this->list)==false) {
                foreach ($this->list as $value) {
                    if ($value->getId() === $article->getId()) {
                        $ui = uniqid("", false);
                        $article->setId($ui);
                    }
                }
            } else {
                throw new EmptyListException();
            }
        } catch (EmptyListException $emptyListException) {
            var_export($emptyListException->getMessage());
        }
    }

    /**
     *add normal article to shop-list
     */
    public function addProduct2()
    {
        try{
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $this->head->header();
        echo "\n";
        echo "\e[1;36;40mSTOCK-MANAGMENT: \e[0m\n" . "\n";
        $input = [];
        echo "\e[0;30;47mTip in product´s:  \e[0m\n" . "\n";
        $input[0] = readline("ID: ");
        while ($input[0] <= 0) {
            $input[0] = readline("ID: ");
        }
        $input[1] = readline("Name: ");
        $input[2] = readline("Price: ");
        while ($input[2] <= 0 || $input[2] == null) {
            $input[2] = readline("Price: ");
        }
        $input[3] = readline("Quantity: ");
        while ($input[3] <= 0 || null) {
            $input[3] = readline("Quantity: ");
        }
        $article = new AnyProduct($input[0], $input[1], $input[2], $input[3]);
        if ($this->productExisting($article->getName()) === false) {
            $this->idControll($article);
            $this->addToList($article);
            $this->mainMenu();
        }
        else{
           throw new DuplicateProductException;
        }}
        catch(DuplicateProductException $d){
            var_export($d->getMessage());
        }

    }

    /**
     *creates a fruit and inserts that to shop-list
     */
    public function addFruitAsProduct()
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        echo "\n";
        echo "\n";
        $input = []; // 0= id 1= name 2=price 3=quantity
        $fruitgarden = new Models\FruitGarden();
        $fruitgarden->addFruit();
        $this->where++;
        if ($this->productExisting($fruitgarden->getFruitsName($this->where - 1)) === false) {
            echo "\e[1;36;40mSTOCK-MANAGMENT: \e[0m\n" . "\n";
            echo "\e[0;30;47mTip in product´s:  \e[0m\n" . "\n";
            $input[0] = readline("[1] ID: ");
            while ($input[0] <= 0) {
                $input[0] = readline("[1] ID: ");
            }
            $input[1] = $fruitgarden->getFruitsName($this->where - 1);
            $input[2] = readline("[2] Price: ");
            while ($input[2] <= 0) {
                $input[2] = readline("[2] Price: ");
            }
            $input[3] = readline("[3] Quantity: ");
            while ($input[3] <= 0) {
                $input[3] = readline("[3] Quantity: ");
            }
            $a = new AnyProduct($input[0], $input[1], $input[2], $input[3]);
            $this->idControll($a);
            $this->addToList($a);
            $this->mainMenu();
        }

    }

    /**
     * @param AnyProduct $a
     * add product to list
     */
    public function addToList(AnyProduct $a)
    {
        $this->list[$a->getId()] = $a;
    }
    /**
     * @param string $name
     */

    /**
     * @param string $name
     * deletes product, if quantity is <=0
     */
    public function deleteProduct(string $name)
    {
        try {
            if ($this->searchProduct($name)->getId() === null) {
                throw new  \FruitStore\Exceptions\NoIdException();
            } else {
                unset($this->list[$this->searchProduct($name)->getId()]);
            }
        } catch (\FruitStore\Exceptions\NoIdException $noIdException) {
            var_export($noIdException->getMessage());
        }
    }

    /**
     * @param string $name
     * @param int $price
     * @return $this
     * searches for product and changes price
     */
    public function changePrice(string $name, int $price)
    {
        try {
            if ($this->productExisting($name)) {
                $this->searchProduct($name)->setPrice($price);
                return $this;
            } else {
                throw new \FruitStore\Exceptions\NoProductExistingException();
            }
        } catch (\FruitStore\Exceptions\NoProductExistingException $noProductExistingException) {
            var_export($noProductExistingException->getMessage());
        }
        return $this;
    }

    /**
     *represents all products in cmd
     */
    public function showProductII()
    {
        try {
            if (empty($this->list) == false) {
                foreach ($this->list as $key => $value) {
                    echo "============================================================" . "\n";
                    echo "\e[0;30;47mProduct´s... \e[0m\n" . "\n";
                    echo "ID: " . $value->getId() . "\n";
                    echo "Name: " . $value->getName() . "\n";
                    echo "Price: " . $value->getPrice() . "\n";
                    echo "Quantity: " . $value->getQuantity() . "\n";
                    echo "============================================================" . "\n";
                }
            } else {
                throw new EmptyShopException();
            }
        } catch (EmptyShopException $e) {
            var_export($e->getMessage());
        }
    }

    /**
     *presents shop-details in cmd
     */
    public function showShopII()
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        echo "\n";
        $this->head->header();
        echo $this;
    }

    /**
     *kind of menue, that enables to choose between several options in cmd
     */
    public function mainMenu()
    {
        $this->head->header();
        $active = true;
        do {
            try {
                echo "\n";
                $option = null;
                echo "\e[1;36;40mMAIN-MENU: \e[0m\n" . "\n";
                echo "\e[0;30;47mEnter correct number for further option: \e[0m\n" . "\n" . "[1] Add product" . "\n" . "[2] Add fruit as product" . "\n" . "[3] Search for an article" . "\n" . "[4] increase products quantity" . "\n" . "[5] Delete a product" . "\n" . "[6] Show shop" . "\n" . "[7] Show all product" . "\n" . "[8] Show both" . "\n" . "[9] Shopping-Cart" . "\n";
                $option = readline("\n"."Your entry: " . "\n");
                switch ($option) {
                    case '1':
                        $this->addProduct2();
                        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                        break;
                    case '2':
                        $this->addFruitAsProduct();
                        break;
                    case '3':
                        $this->searchProductInteractive();
                        break;
                    case '4':
                        try {
                            if (empty($this->list) == true) {
                                throw new NoProductExistingException;
                            } else {
                                $this->increaseQuantity();
                            }
                        } catch (NoProductExistingException $n) {
                            var_export($n->getMessage());
                        }
                        break;
                    case '5':
                        $this->deleteProductInteractive();
                        break;
                    case '6':
                        $this->showShopII();
                        break;
                    case '7':
                        $this->showProductII();
                        break;
                    case '8':
                        $this->showShopII();
                        $this->showProductII();
                        break;
                    case '9':
                        $this->cartMenu();
                        break;
                    default:
                        throw new UnexpectedNumberException();
                        break;
                }
            } catch (UnexpectedNumberException $e) {
                var_export($e->getMessage());
            }
        } while ($active === true);
    }

    /**
     * @param $crtnmbr
     * menu to manage carts input
     */
    public function menue($crtnmbr)
    {
       // echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $this->head->header();
        $active = true;
        do {
            try {
                echo "\n";
                $option = null;
                echo "\e[1;36;40mCART-MENU: \e[0m\n" . "\n";
                echo "\e[0;30;47mEnter correct number for further option: \e[0m\n" . "\n" . "[1] Add product to cart" . "\n" . "[2] Remove product from cart" . "\n" . "[3] Show cart´s content" . "\n" . "[4] Show bill" . "\n" . "[5] Back to FruitStore" . "\n";
                echo "\n";
                $option = readline("Your Entry: " . "\n");
                echo "\n";
                switch ($option) {
                    case '1':
                        {
                            try {
                                if (empty($this->list) === false) {
                                    $this->addArticleToCart($crtnmbr);
                                    $this->menue($crtnmbr);
                                    break;
                                } else {
                                    throw new NoProductExistingException();
                                }
                            } catch (NoProductExistingException $e) {
                                var_export($e->getMessage());
                            }
                            break;
                        }
                    case '2':
                        {
                            $this->removeArticleFromCart($crtnmbr);
                            $this->menue($crtnmbr);
                            break;
                        }
                    case '3':
                        {
                            $this->showCartInput($crtnmbr);
                            $this->menue($crtnmbr);
                            break;
                        }
                    case '4':
                        {
                            try {
                                if (empty($this->cart[$crtnmbr]->getContent()) == false) {
                                    $this->checkingOut($crtnmbr);

                                } else {
                                    throw new EmptyCartException();
                                }
                            } catch (EmptyCartException $empty) {
                                var_export($empty->getMessage());
                            }
                            break;
                        }
                    case '5':
                        {
                            $active = false;
                            $this->mainMenu();
                            break;
                        }
                    default:
                        throw new UnexpectedNumberException();
                        break;
                }
            } catch (UnexpectedNumberException $e) {
                var_export($e->getMessage());
            }
        } while ($active === true);
    }

    /**
     * menu to select or create cart
     */
    public function cartMenu()
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $active = true;
        do {
            try {
                echo "\n";
                $this->head->header();
                $option = null;
                echo "\e[1;36;40mCART-SELECT-MENU: \e[0m\n" . "\n";
                echo "\e[0;30;47mShopping-Cart-menu: \e[0m\n" . "\n" . "[1] Get a new shopping-cart" . "\n" . "[2] Select a cart" . "\n" . "[3] Go back to shop" . "\n";
                $option = readline("Your Entry: " . "\n");
                echo "\n";
                switch ($option) {
                    case '1':
                        {
                            echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                            echo "\n";
                            $this->head->header();
                            echo "\e[1;36;40mCART-CREATION: \e[0m\n" . "\n";
                            echo "\e[0;30;47mSelect number for your shopping-cart: \e[0m\n";
                            echo "\n";
                            $number = readline("Your Entry: " . "\n");
                            while ($this->isCrtNmbrInArr($number) === true || $number === null) {
                                echo "Error: You need to choose another cart-number!";
                                $number = readline("Your Entry: " . "\n");

                            }
                            echo "\n";
                            $shoppingCart = new Cart();
                            $shoppingCart->setCartNbr($number);
                            $this->cart[$number] = $shoppingCart;
                            $this->menue($shoppingCart->getCartNbr());
                            break;
                        }
                    case '2':
                        {
                            try {
                                if (empty($this->cart) == false) {
                                    echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                                    echo"\n";
                                    $this->head->header();
                                    echo "\e[1;36;40mCART-SELECTION: \e[0m\n" . "\n";
                                    echo "\e[0;30;47mSelect a cart: \e[0m\n" . "\n";
                                    foreach ($this->cart as $key => $value) {
                                        echo "[" . $value->getCartNbr() . "]" . "\n";
                                    }
                                    $option2 = readline("Your Entry: " . "\n");
                                    while ($this->isCrtNmbrInArr($option2) === false || $option2 === null) {
                                        $option2 = readline("Your Entry: " . "\n");
                                    }
                                    $this->menue($option2);
                                } else {
                                    throw new NoCartException();
                                }
                            } catch (NoCartException $n) {
                               // echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                                var_export($n->getMessage());
                            }
                            $this->cartMenu();
                            break;
                        }
                    case '3':
                        {
                            $active = false;
                            echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                            $this->mainMenu();
                            break;
                        }
                    default:
                        throw new UnexpectedNumberException();
                        break;
                }
            } catch (UnexpectedNumberException $e) {
                var_export($e->getMessage());
            }
        } while ($active === true);
    }

    /**
     * @param $int = cartnumber
     * copies products to belt and deletes cart
     */
    public function addProductToBelt($int)
    {
        if (empty($this->cart[$int]) === false) { // wenn produkte im array des wk sind
            // wenn etwas auf dem laufband liegt
            foreach ($this->cart[$int]->getContent() as $key => $value) { // für alle objecte des arrays belt
                if ($this->checkout->getBelt()[$value->getId()] !== null) {
                    $this->checkout->getBelt()[$value->getId()]->increaseQuantity($value->getQuantity());
                } else {
                    $prod = new AnyProduct($value->getId(), $value->getName(), $value->getPrice(), $value->getQuantity());
                    $this->checkout->setBelt($prod->getId(), $prod);
                }
            }
            unset($this->cart[$int]);
        } else {
            echo "Error: Selected shopping-cart is empty!" . "\n";
        }
    }

    /**
     * @return float|int
     * calculates price of belt
     */
    public function calculatePrice()
    {
        $cash = 0;
        try {
            if (empty($this->checkout->getBelt()) === false) {
                foreach ($this->checkout->getBelt() as $key => $value) {
                    $cashi = $value->getPrice() * $value->getQuantity();
                    $cash += $cashi;
                }
                return $cash;
            } else {
                throw new EmptyListException();
            }
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }

    /**
     * @param $cn
     * little menu to select other carts to checkout later
     */
    public function checkingOut($cn)
    {
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        $active = 0;
        $help = [];
        do {
            $active = true;
            echo "\n";
            $this->head->header();
            echo "\e[1;36;40mCHECKOUT: \e[0m\n" . "\n";
            echo "\e[0;30;47mWant you to pay for another cart?  \e[0m\n" . "\n";
            echo "\n";
            echo "[1] Yes" . "\n" . "[2] No" . "\n";
            echo "\n";
            $choice = readline("Your entry:");
            while (2 < $choice || $choice < 1) {
                $choice = readline("Your entry:");
            }
            switch ($choice) {
                case 2:
                    {
                        $this->addProductToBelt($cn);
                        $this->showBillAtCheckOut();
                        $active = false;
                        $this->checkout->getBelt() === null;
                        $this->mainMenu();
                        break;
                    }
                case 1:
                    {
                        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
                        $this->addProductToBelt($cn);
                        if (empty($this->cart == false)) {
                            echo "\e[0;30;47mSelect a cart: \e[0m\n" . "\n";
                            foreach ($this->cart as $key => $value) {
                                if ($value->getCartNbr() != $cn) {
                                    echo "[" . $value->getCartNbr() . "]" . "\n";
                                }
                            }
                            $choice2 = readline("Your Entry: " . "\n");
                            while ($this->isCrtNmbrInArr($choice2) === false || $choice2 === null) {
                                $choice2 = readline('Your Entry: ' . "\n");
                            }
                            $this->addProductToBelt($choice2);
                        }
                        break;
                    }
            }
        } while ($active === true);
    }

    /**
     * collection of methods to get to the bill
     */
    public function showBillAtCheckOut()
    {
        $this->calculateBackMoney();
        $this->showShopII();
        $this->showProductsAtCheckOut();
        $this->checkout->getBelt() === null;
    }

    /**
     * calculates rest
     */
    public function calculateBackMoney()
    {
        $i = 0;
        $activePosRest = false;
        $activeBiggerNull = false;
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        echo "\n";
        echo "\e[1;36;40mCHECKOUT: \e[0m\n" . "\n";
        echo "\e[0;30;47mMoney-management\e[0m\n" . "\n";
        echo "\n";
        echo "Your costs are: " . $this->calculatePrice() . "$" . "\n";
        echo "\n";
        while ($activeBiggerNull === false || $activePosRest === false) {
            echo "\n";
            $i = readline("Your given amount of money: ");
            echo "\n";
            while ($i == null) {
                echo "\n";
                $i = readline("Your given amount of money: ");
                echo "\n";
            }
            if ($this->calculatePrice() - $i <= 0) {
                $activePosRest = true;
            } else {
                $activePosRest = false;
                echo "\n";
                echo "\e[1;33;40mError: You gave too less money!\e[0m\n";
                echo "\n";
            }
            if ($i > 0) {
                $activeBiggerNull = true;
            } else {
                $activeBiggerNull = false;
                echo "\n";
                echo "\e[1;33;40mError: You gave a negative amount of money OR nothing!\e[0m\n";
                echo "\n";
            }
        }
        $rest = $i - $this->calculatePrice();
        $this->backMoney = $rest;
    }

    /**
     * presents every product, laying on belt
     */
    public function showProductsAtCheckOut()
    {
        try {
            if (empty($this->checkout->getBelt()) === false) {
                echo "============================================================" . "\n";
                echo "\e[0;30;47mProduct´s... \e[0m\n" . "\n";
                foreach ($this->checkout->getBelt() as $key => $value) {
                    echo "[" . $value->getId() . "] ";
                    echo "Name: " . $value->getName();
                    echo " Price: " . $value->getPrice();
                    echo " Quantity: " . $value->getQuantity() . "\n";
                }
                echo "============================================================" . "\n";
                echo "Payable amount: " . $this->calculatePrice() . "\n";
                $given = $this->backMoney + $this->calculatePrice();
                echo "Your given money: " . $given . "$" . "\n";
                echo "Rest: " . $this->backMoney . "$" . "\n";
                echo "\n";
                echo "\n";
                echo "\n";
            } else {
                throw new EmptyListException();
            }
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }
}
