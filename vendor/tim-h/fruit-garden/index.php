<?php
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 08.01.2019
 * Time:
 */

require_once(__DIR__ . '/vendor/autoload.php'); // autoloader wird nur einmal eingepflegt; Dir gibt den weg zum richtigen Verzeichnis


$fruitgarden=new \FruitGarden\Models\FruitGarden();
$fruitgarden->addFruit();
$fruitgarden->showFruitsII();


/**
$fruitGarden1=new \FruitGarden\Models\FruitGarden();
$fruitGarden1
        ->addFruit($fruit1)
        ->addFruit($fruit0)
        ->addFruit($fruit2)
        ->addFruit($fruit3)
        ->addFruit($fruit4);
**/
