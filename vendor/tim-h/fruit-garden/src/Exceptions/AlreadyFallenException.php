<?php
namespace FruitGarden\Exceptions;
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 09.01.2019
 * Time: 11:56
 */
class AlreadyFallenException extends \Exception {
    public function __construct()
    {
        $this->message="\n". "\e[1;33;40mError: Fruit is already fallen!\e[0m\n"."\n";
        parent::__construct();
    }
}