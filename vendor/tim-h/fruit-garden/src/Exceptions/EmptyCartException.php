<?php
namespace FruitStore\Exceptions;
/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 09.01.2019
 * Time: 11:56
 */
class EmptyCartException extends \Exception {
    public function __construct()
    {
        $this->message="\n". "\e[1;33;40mError: The cart is empty or the product does not exist!\e[0m\n"."\n";
        parent::__construct();
    }
}