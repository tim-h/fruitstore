<?php

namespace FruitGarden\Models;

use \Exception;
use FruitGarden\Exceptions\NotRipeException;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 08.01.2019
 * Time: 15:44
 */
abstract class AbstractBushFruit extends AbstractFruit
{
    public function __construct($name, $color = null, $origin = null, $type = null)
    {
        parent::__construct($name, $color, $origin, $type);
        $this->increasedTimeMin = 6;
        $this->increasedTimeMax = 12;
    }

    public function pickUp() //test, that fruit is ripe
    {
        try{
            if($this->getRipe()===true){
                $this->setPickedUp(true);
            }else{
                throw new NotRipeException();
            }
        }
        catch(NotRipeException $notRipeException){
            var_export($notRipeException->getMessage());
        }
    }

    public function show()
    {
        echo $this->getName();
        echo $this->getOrigin();
        echo $this->getColor();
        echo $this->getType();
        echo $this->getPickedUp();
    }

    public function createRest($origin,$color,$type,$selected,$month){
        $this->setOrigin($origin);
        $this->setColor($color);
        $this->setType($type);
        $this->setPickedUp($selected);
        $this->setActualMonth($month);
        if($this->getActualMonth()<=$this->increasedTimeMax&&$this->getActualMonth()>$this->increasedTimeMin){
            $this->setRipe(true);
        }
        else{
            $this->setRipe(false);
        }
        return $this;
    }

    public function showInTable()
    {
        if ($this->pickedUp === true) {
            $this->htmlSelected = 'true';
        } else {
            $this->htmlSelected = 'false';
        }
        echo "<style type=\"text/css\">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
</style><br /><br />
<table class=\"tg\">
  <caption><b>$this->type</b></caption>
  <tr  bgcolor=\"#E6E6E6\">
    <th class=\"tg-031e\">Name</th>
    <th class=\"tg-031e\">Origin</th>
    <th class=\"tg-031e\">Color</th>
    <th class=\"tg-031e\">Type</th>
    <th class=\"tg-031e\">Selected?</th>
    <th class=\"tg-031e\">Ripe?</th>
    <th class=\"tg-031e\">Month</th>
    
  </tr>
  <tr>
    <td class=\"tg-031e\">$this->name</td>
    <td class=\"tg-031e\">$this->origin</td>
    <td class=\"tg-031e\">$this->color</td>
    <td class=\"tg-031e\">$this->type</td>
    <td class=\"tg-031e\">$this->htmlSelected</td>
    <td class=\"tg-031e\">$this->htmlRipe</td>
    <td class=\"tg-031e\">$this->actualMonth</td>
  </tr>

</table>

    ";
    }
}