<?php

namespace FruitGarden\Models;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 08.01.2019
 * Time: 13:56
 */
abstract class AbstractFruit
{
    protected $name;
    protected $color;
    protected $origin;
    protected $type;
    protected $increasedTimeMin; // int in months
    protected $increasedTimeMax;
    protected $pickedUp;
    protected $ripe;
    protected $htmlSelected;
    protected $actualMonth;
    protected $htmlRipe;

    public function __construct($name, $color = null, $origin = null, $type = null,$pickedUp=null,$ripe=null, $actualMonth=null)
    {
        $this->name = $name;
        $this->color = $color;
        $this->origin = $origin;
        $this->type = $type;
        $this->pickedUp=$pickedUp;
        $this->ripe=$ripe;
        $this->actualMonth=$actualMonth;
    }

    public function getActualMonth(){
        return $this->actualMonth;
    }

    public function setActualMonth($month){
        $this->actualMonth=$month;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getOrigin()
    {
        return $this->origin;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPickedUp()
    {
        return $this->pickedUp;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function setPickedUp($boolean)
    {
        $this->pickedUp = $boolean;
        return $this;
    }

    public function getRipe(){
        return $this->ripe;
    }

    public function setRipe($boolean){
        $this->ripe=$boolean;
        if($this->getRipe()===true){
            $this->htmlRipe='true';
        }
        else{
            $this->htmlRipe='false';
        }
        return $this;
}
}