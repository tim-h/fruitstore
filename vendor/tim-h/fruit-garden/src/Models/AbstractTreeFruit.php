<?php

namespace FruitGarden\Models;

use \Exception;
use FruitGarden\Exceptions\AlreadyFallenException;
use FruitGarden\Exceptions\LadderMissingException;
use FruitGarden\Exceptions\NotRipeException;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 08.01.2019
 * Time: 15:16
 */
abstract class AbstractTreeFruit extends AbstractFruit
{
    protected $ladderAvailable;
    protected $alreadyFallen;
    protected $htmlAlreadyFallen;
    protected $htmlLadderAvailable;

    public function __construct($name, $color = null, $origin = null, $type = null)
    {
        parent::__construct($name, $color, $origin, $type);
        $this->increasedTimeMin = 12;
        $this->increasedTimeMax = 24;
    }

    public function pickUp() //test, that fruit is ripe
    {
        try {
            if ($this->getRipe() === true) {
                try {
                    if ($this->getLadderAvailable() === true) {
                        try {
                            if ($this->getAlreadyFallen() === false) {
                                $this->setPickedUp(true);
                            } else {
                                throw new AlreadyFallenException();
                            }
                        } catch (AlreadyFallenException $alreadyFallenException) {
                            var_export($alreadyFallenException->getMessage());
                        }
                    } else {
                        throw new LadderMissingException();
                    }
                } catch (LadderMissingException $ladderMissingException) {
                    var_export($ladderMissingException->getMessage());
                }
            } else {
                throw new NotRipeException();
            }
        } catch (NotRipeException $notRipeException) {
            var_export($notRipeException->getMessage());
        }
    }

    public function show()
    {
        echo $this->getName();
        echo $this->getOrigin();
        echo $this->getColor();
        echo $this->getType();
        echo $this->getPickedUp();
        echo $this->getLadderAvailable();
    }

    public function getAlreadyFallen()
    {
        return $this->alreadyFallen;
    }

    public function setAlreadyFallen($boolean)
    {
        $this->alreadyFallen = $boolean;
        if ($this->alreadyFallen === true) {
            $this->pickedUp = true;
        }

        return $this;
    }

    public function setLadderAvailable($boolean)
    {
        $this->ladderAvailable = $boolean;
        return $this;
    }

    public function getLadderAvailable()
    {
        return $this->ladderAvailable;
    }


    public function createRest($origin, $color, $type, $selected, $ladderAvailable, $alreadyFallen, $month)
    {
        $this->setOrigin($origin);
        $this->setColor($color);
        $this->setType($type);
        $this->setPickedUp($selected);
        $this->setAlreadyFallen($alreadyFallen);
        $this->setLadderAvailable($ladderAvailable);
        $this->setActualMonth($month);
        if ($this->pickedUp === true) {
            $this->setAlreadyFallen(true);
        }
        if ($this->getActualMonth() <= $this->increasedTimeMax && $this->getActualMonth() > $this->increasedTimeMin) {
            $this->setRipe(true);
        } else {
            $this->setRipe(false);
        }
        return $this;
    }


    public function showInTable()
    {

        if ($this->pickedUp === true) {
            $this->htmlSelected = 'true';
        } else {
            $this->htmlSelected = 'false';
        }
        if ($this->alreadyFallen === true) {
            $this->htmlAlreadyFallen = 'true';
        } else {
            $this->htmlAlreadyFallen = 'false';
        }
        if ($this->ladderAvailable === true) {
            $this->htmlLadderAvailable = 'true';
        } else {
            $this->htmlLadderAvailable = 'false';
        }
        if ($this->ripe === true) {
            $this->htmlRipe = 'true';
        } else {
            $this->htmlRipe = 'false';
        }

        echo "<style type=\"text/css\">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
</style><br /><br />
<table class=\"tg\">
  <caption><b>$this->type</b></caption>
  <tr bgcolor=\"#E6E6E6\" >
    <th class=\"tg-031e\">Name</th>
    <th class=\"tg-031e\">Origin</th>
    <th class=\"tg-031e\">Color</th>
    <th class=\"tg-031e\">Type</th>
    <th class=\"tg-031e\">Selected?</th>
    <th class=\"tg-031e\">Ladder available?</th>
    <th class=\"tg-031e\">Already fallen?</th>
    <th class=\"tg-031e\">Ripe?</th>
    <th class=\"tg-031e\">Month</th>
  </tr>
  <tr>
    <td class=\"tg-031e\">$this->name</td>
    <td class=\"tg-031e\">$this->origin</td>
    <td class=\"tg-031e\">$this->color</td>
    <td class=\"tg-031e\">$this->type</td>
    <td class=\"tg-031e\">$this->htmlSelected</td>
    <th class=\"tg-031e\">$this->htmlLadderAvailable</th>
    <th class=\"tg-031e\">$this->htmlAlreadyFallen</th>
    <th class=\"tg-031e\">$this->htmlRipe</th>
    <th class=\"tg-031e\">$this->actualMonth</th>
  </tr>
</table>
    ";
    }
}