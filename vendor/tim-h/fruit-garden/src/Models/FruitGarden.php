<?php

namespace FruitGarden\Models;

use FruitStore\Exceptions\DuplicateProductException;
use FruitStore\Exceptions\NoProductExistingException;
use FruitStore\Exceptions\NoIdException;
use FruitStore\Exceptions\EmptyListException;
use FruitStore\Models\Intro;

/**
 * Created by PhpStorm.
 * User: Jtl
 * Date: 08.01.2019
 * Time: 14:08
 */
class FruitGarden
{
    private $fruits = [];
    private $head;

    //returns name of a certain fruit
    //parameter is fruits-key
    public function getFruitsName($a)
    {
        return $this->fruits[$a]->getName();
    }
    /**
     * Wahrheitswerte müssen mit 0= false und 1 =true eingegeben werden
    interactive method to add a fruit to fruitlist
    avoids to tip in wrong values
     */
    public function addFruit()
    {
        $this->head=new Intro();
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        echo "\n";
        $this->head->header();
        $input;
        $details = [];
        echo "\e[1;36;40mFRUIT-CREATION: \e[0m\n" . "\n";
        echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" . "\n";
        echo "\e[0;30;47mChoose the type of fruit:  \e[0m\n" . "\n";
        echo "[1] Berry" . "\n" . "[2] Nuclear fruit" . "\n" . "[3] Southern fruit" . "\n";
        echo "\n";
        $input = readline("Your Entry: " . "\n");
        echo "\n";
        while ($input <= 0 || $input > 3 || null) {
            $input = readline("Your entry: ");
        }
        echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" . "\n";
        if ($input == 1) {
            echo "\e[0;30;47mGive more details about the fruit:  \e[0m\n" . "\n";
            $details[0] = readline("[1] Name :  " . "\n");
            $details[1] = readline("[2] Origin :  " . "\n");
            $details[2] = readline("[3] Color :  " . "\n");
            echo "[4] Selected : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[3] = readline("Your entry: ");
            echo "\n";
            while (2 < $details[3] || $details[3] < 1) {
                echo "[4] Selected : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[3] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[3] == 1) {
                $details[3] = null;
                $details[3] = true;
            } elseif ($details[3] == 2) {
                $details[3] = false;
            }
            $details[4] = readline("[5] Month :  " . "\n");
            while($details[4]<=0){
                $details[4] = readline("[5] Month :  " . "\n");
            }
            $fruit = new Berry($details[0]);
            $fruit->createRest($details[1], $details[2], "Berry", $details[3], $details[4]);
            $this->fruits[] = $fruit;
        }

        if ($input == 2) {
            echo "\e[0;30;47mGive more details about the fruit:  \e[0m\n" . "\n";
            $details[0] = readline("[1] Name :  " . "\n");
            $details[1] = readline("[2] Origin :  " . "\n");
            $details[2] = readline("[3] Color :  " . "\n");
            echo "[5] Ladder available : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[4] = readline("Your entry: ");
            echo "\n";
            while (2 < $details[4] || $details[4] < 1) {
                echo "[5] Ladder available : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[4] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[4] == 1) {
                $details[4] = true;
            } elseif ($details[4] == 2) {
                $details[4] = false;
            }
            echo "[6] Fruit already fallen : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[5] = readline("Your entry:");
            echo "\n";
            while (2 < $details[5] || $details[5] < 1) {
                echo "[6] Fruit already fallen : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[5] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[5] == 1) {
                $details[5] = true;
            } elseif ($details[5] == 2) {
                $details[5] = false;
            }
            echo "[7] Fruit already selected: " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[6] = readline("Your entry:");
            echo "\n";
            while (2 < $details[6] || $details[6] < 1) {
                echo "[7] Fruit already selected : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[6] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[6] == 1) {
                $details[6] = true;
            } elseif ($details[6] == 2) {
                $details[6] = false;
            }
            $details[7] = readline("[8] Month :  " . "\n");
            while($details[7]<=0){
                $details[7] = readline("[8] Month :  " . "\n");
            }
            $fruit = new NuclearFruit($details[0]);
            $fruit->createRest($details[1], $details[2], "Nuclear fruit", $details[6], $details[4], $details[5], $details[7]);
            $this->fruits[] = $fruit;
        }
        if ($input == 3) {
            echo "\e[0;30;47mGive more details about the fruit:  \e[0m\n" . "\n";
            $details[0] = readline("[1] Name :  " . "\n");
            $details[1] = readline("[2] Origin :  " . "\n");
            $details[2] = readline("[3] Color :  " . "\n");
            echo "[4] Selected :  " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[3] = readline("Your entry:");
            echo "\n";
            while (2 < $details[3] || $details[3] < 1) {
                echo "[4] Selected :  " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[3] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[3] == 1) {
                $details[3] = true;
            } elseif ($details[3] == 2) {
                $details[3] = false;
            }
            echo "[5] Ladder available : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[4] = readline("Your entry: ");
            echo "\n";
            while (2 < $details[4] || $details[4] < 1) {
                echo "[5] Ladder available : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[4] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[4] == 1) {
                $details[4] = true;
            } elseif ($details[4] == 2) {
                $details[4] = false;
            }
            echo "[6] Fruit already fallen : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
            echo "\n";
            $details[5] = readline("Your entry: ");
            echo "\n";
            while (2 < $details[5] || $details[5] < 1) {
                echo "[6] Fruit already fallen : " . "\n" . "     [1] Yes" . "\n" . "     [2] No" . "\n";
                echo "\n";
                $details[5] = readline("Your entry: ");
                echo "\n";
            }
            if ($details[4] == 1) {
                $details[4] = true;
            } elseif ($details[4] == 2) {
                $details[4] = false;
            }
            $details[6] = readline("[7] Month :  " . "\n");
            while($details<=0){
                $details[6] = readline("[7] Month :  " . "\n");
            }
            $fruit = new SouthernFruit($details[0]);
            $fruit->createRest($details[1], $details[2], "Southern fruit", $details[3], $details[4], $details[5], $details[6]);
        }
        $this->fruits[] = $fruit;
        echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" . "\n";
        echo chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J';
        return $this;
    }

    public function searchFruit($name, $getKey = false)
    {
        /** @var Fruit $value */
        foreach ($this->fruits as $key => $value) {
            if ($value->getName() === $name) {
                if ($getKey) {
                    return $key;
                } else {
                    return $value;
                }
            }
        }
        return null;
    }

    public function deleteFruit($name)
    {
        unset($this->fruits[$this->searchFruit($name, true)]);
        return $this;
    }

// Wahrheitswerte werden als zahlen ausgegeben: 0=false, 1=true
    public function showFruitsII()
    {
        echo "\n";  echo "\n";
        try {
            if ($this->fruits !== null) {
                foreach ($this->fruits as $key => $value) {
                    if ($value->getType() === "Berry") {
                        $this->htmlSelected = $value->getPickedUp();
                        echo "____________________________________________________________" . "\n";
                        echo "Fruit..." . "\n";
                        echo "Name: " . $value->getName() . "\n";
                        echo "Type: " . $value->getType() . "\n";
                        echo "Color: " . $value->getColor() . "\n";
                        echo "Selected: " . $value->getPickedUp() . "\n";
                        echo "Month: " . $value->getActualMonth() . "\n";
                        echo "____________________________________________________________" . "\n";
                    }
                    if ($value->getType() === "Nuclear fruit" || $value->getType() === "Southern fruit") {
                        echo "____________________________________________________________" . "\n";
                        echo "Fruit..." . "\n";
                        echo "Name: " . $value->getName() . "\n";
                        echo "Type: " . $value->getType() . "\n";
                        echo "Color: " . $value->getColor() . "\n";
                        echo "Selected: " . $value->getPickedUp() . "\n";
                        echo "Ladder available: " . $value->getLadderAvailable() . "\n";
                        echo "Already fallen: " . $value->getAlreadyFallen() . "\n";
                        echo "Month: " . $value->getActualMonth() . "\n";
                        echo "____________________________________________________________" . "\n";
                    }
                }
            } else {
                throw new EmptyListException();
            }
        } catch (EmptyListException $e) {
            var_export($e->getMessage());
        }
    }

}